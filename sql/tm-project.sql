-- Table: public.tm_project

-- DROP TABLE IF EXISTS public.tm_project;

CREATE TABLE IF NOT EXISTS public.tm_project
(
    id character varying(100) COLLATE pg_catalog."default" NOT NULL,
    created time without time zone NOT NULL,
    name character varying(200) COLLATE pg_catalog."default" NOT NULL,
    descrptn character varying(1000) COLLATE pg_catalog."default" NOT NULL,
    status character varying(50) COLLATE pg_catalog."default" NOT NULL,
    user_id character varying(100) COLLATE pg_catalog."default",
    CONSTRAINT tm_project_pk PRIMARY KEY (id),
    CONSTRAINT user_id_fk FOREIGN KEY (user_id)
        REFERENCES public.tm_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_project
    OWNER to postgres;

COMMENT ON TABLE public.tm_project
    IS 'TASK MANAGER PROJECTS TABLE';