package ru.t1.gorodtsova.tm.api.service;

import java.sql.Connection;

public interface IConnectionService {

    Connection getConnection();

}
